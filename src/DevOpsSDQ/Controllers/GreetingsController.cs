﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DevOpsSDQ.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GreetingsController : ControllerBase
    {
        // GET: api/greetings
        [HttpGet]
        public string Get()
        {
            return $"Hello world!";
        }

        // GET api/greetings/alberto
        [HttpGet("{name}")]
        public string Get(string name)
        {
            return $"Hello {name}";
        }

        // POST api/greetings
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/greetings/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/greetings/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
